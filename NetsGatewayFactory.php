<?php

namespace Empatix\Nets;

use Payum\Core\GatewayFactory;
use Empatix\Nets\Action\StatusAction;
use Empatix\Nets\Action\RefundAction;
use Empatix\Nets\Action\CancelAction;
use Empatix\Nets\Action\NotifyAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Empatix\Nets\Action\CaptureAction;
use Empatix\Nets\Action\AuthorizeAction;
use Empatix\Nets\Action\ConvertPaymentAction;

class NetsGatewayFactory extends GatewayFactory
{
    /**
     * {@inheritDoc}
     */
    protected function populateConfig(ArrayObject $config)
    {
        $config->defaults([
            'payum.factory_name' => 'nets',
            'payum.factory_title' => 'nets',
            'payum.action.refund' => new RefundAction(),
            'payum.action.cancel' => new CancelAction(),
            'payum.action.notify' => new NotifyAction(),
            'payum.action.status' => new StatusAction(),
            'payum.action.capture' => new CaptureAction(),
            'payum.action.authorize' => new AuthorizeAction(),
            'payum.action.convert_payment' => new ConvertPaymentAction(),
        ]);

        if (false == $config['payum.api']) {
            $config['payum.default_options'] = [
                'password' => '',
                'sandbox' => true,
                'merchantId' => '',
            ];

            $config->defaults($config['payum.default_options']);

            $config['payum.required_options'] = ['merchantId', 'password'];

            $config['payum.api'] = function(ArrayObject $config) {
                $config->validateNotEmpty($config['payum.required_options']);

                return new Api((array) $config, $config['payum.http_client'], $config['httplug.message_factory']);
            };
        }
    }
}
