<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'Payum\\Core\\' => array($vendorDir . '/payum/core'),
    'Empatix\\Nets' => array($baseDir . '/'),
);
